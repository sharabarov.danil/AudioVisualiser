﻿namespace AudioVisualization
{
    /// <summary>
    /// Interface for Amplitude
    /// </summary>
    public interface IAmplitude
    {
        /// <summary>
        /// Get Amplitude
        /// </summary>
        /// <param name="audioBands"></param>
        /// <returns></returns>
        float GetAmplitude(float[] audioBands);
    }
}
