﻿using UnityEngine;

namespace AudioVisualization
{
    /// <summary>
    /// Amplitude Emission
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class AmplitudeEmissionVisualizer : AmplitudeVisualizerBase
    {
        [SerializeField]
        private Vector3 _rgbPower;

        private Material _material;

        private void Start()
        {
            _material = GetComponent<MeshRenderer>().materials[0];
        }

        protected override void SetView()
        {
            Color color = new(_rgbPower.x * _audioAmplitudeValue, _rgbPower.y * _audioAmplitudeValue, _rgbPower.z * _audioAmplitudeValue);
            _material.SetColor("_EmissionColor", color);
        }
    }
}
