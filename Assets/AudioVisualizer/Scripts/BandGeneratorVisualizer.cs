﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Example with Cube Generation
    /// </summary>
    public class BandGeneratorVisualizer : MonoBehaviour, IAudioVisualizer
    {
        [SerializeField]
        private GameObject[] _bandCubePrefab;
        [SerializeField]
        private float _radius = 10;

        private AudioVisualizer _audioVisualizer;
        private GameObject[] _bandCube = new GameObject[8];

        public void SetAudioVisualizer(AudioVisualizer audioVisualizer)
        {
            _audioVisualizer = audioVisualizer;
        }

        private void Start()
        {
            for (int i = 0; i < _bandCube.Length; i++)
            {
                GameObject instanceSampleCube = Instantiate(_bandCubePrefab[i % _bandCubePrefab.Length]);
                instanceSampleCube.transform.localPosition = this.transform.localPosition;
                instanceSampleCube.transform.parent = this.transform;
                instanceSampleCube.name = "BandCube" + i;
                instanceSampleCube.transform.localPosition = new Vector3(Mathf.Cos(Mathf.Deg2Rad * (-360f * i) / (float)_bandCube.Length), 0, Mathf.Sin(Mathf.Deg2Rad * (-360f * i) / (float)_bandCube.Length)) * _radius;
                instanceSampleCube.transform.localEulerAngles = Vector3.zero;
                _bandCube[i] = instanceSampleCube;
                var bandVisualizers = instanceSampleCube.GetComponentsInChildren<BandVisualizerBase>();
                for (int j = 0; j < bandVisualizers.Length; j++)
                {
                    bandVisualizers[j].Band = i;
                    bandVisualizers[j].SetAudioVisualizer(_audioVisualizer);
                }
            }
        }
    }
}
