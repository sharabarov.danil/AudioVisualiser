﻿
namespace AudioVisualization
{
    /// <summary>
    /// Band Buffer
    /// </summary>
    public class BandBuffer : IBandBuffer
    {
        private float[] _bufferDecrease = new float[8];

        public float[] GetBandBuffer(float[] frequencyBands)
        {
            float[] bandBuffer = new float[frequencyBands.Length];

            for (int i = 0; i < frequencyBands.Length; i++)
            {
                if (frequencyBands[i] > bandBuffer[i])
                {
                    bandBuffer[i] = frequencyBands[i];
                    _bufferDecrease[i] = 0.005f;
                }

                if (frequencyBands[i] < bandBuffer[i])
                {
                    bandBuffer[i] -= _bufferDecrease[i];
                    _bufferDecrease[i] *= 1.2f;
                }
            }

            return bandBuffer;
        }
    }
}
