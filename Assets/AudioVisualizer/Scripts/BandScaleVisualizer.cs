﻿using UnityEngine;

namespace AudioVisualization
{
    /// <summary>
    /// Band Scale
    /// </summary>
    public class BandScaleVisualizer : BandVisualizerBase
    {
        [SerializeField]
        private float _startScale;
        [SerializeField]
        private float _startMultiplier;
        [SerializeField]
        private bool _xScale;
        [SerializeField]
        private bool _yScale;
        [SerializeField]
        private bool _zScale;

        protected override void SetView()
        {
            transform.localScale = new Vector3(_xScale ? (_audioBandValue * _startMultiplier) + _startScale : transform.localScale.x, _yScale ? (_audioBandValue * _startMultiplier) + _startScale : transform.localScale.y, _zScale ? (_audioBandValue * _startMultiplier) + _startScale : transform.localScale.z);
        }
    }
}
