﻿using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Samples from AudioSource
    /// </summary>
    public class AudioSourceSamples : IAudioSamples
    {
        private AudioSource _audioSource;
        private int _channel;
        private FFTWindow _fftWindow;
        public AudioSourceSamples(AudioSource audioSource, int channel, FFTWindow fftWindow)
        {
            _audioSource = audioSource;
            _channel = channel;
            _fftWindow = fftWindow;
        }
        public float[] GetSamples()
        {
            float[] samples = new float[512];
            _audioSource.GetSpectrumData(samples, _channel, _fftWindow);
            return samples;
        }
    }
}
