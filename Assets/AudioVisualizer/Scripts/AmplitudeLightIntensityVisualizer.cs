﻿
using UnityEngine;

namespace AudioVisualization
{
    /// <summary>
    /// Change Intensity from Band
    /// </summary>
    [RequireComponent(typeof(Light))]
    public class AmplitudeLightIntensityVisualizer : AmplitudeVisualizerBase
    {
        [SerializeField]
        private float _minIntensity;
        [SerializeField]
        private float _maxIntensity;

        private Light _light;

        private void Start()
        {
            _light = GetComponent<Light>();
        }

        protected override void SetView()
        {
            _light.intensity = (_audioAmplitudeValue * (_maxIntensity - _minIntensity)) + _minIntensity;
        }
    }
}
