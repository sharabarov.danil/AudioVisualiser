﻿namespace AudioVisualization
{
    /// <summary>
    /// Interface for band buffer
    /// </summary>
    public interface IBandBuffer
    {
        /// <summary>
        /// Get Band buffer
        /// </summary>
        /// <param name="frequencyBands"></param>
        /// <returns></returns>
        float[] GetBandBuffer(float[] frequencyBands);
    }
}
