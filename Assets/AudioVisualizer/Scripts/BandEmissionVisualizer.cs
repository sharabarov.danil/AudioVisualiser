﻿using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Band Emission
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class BandEmissionVisualizer : BandVisualizerBase, IAudioVisualizer
    {
        [SerializeField]
        private Vector3 _rgbPower = Vector3.one;

        private Material _material;

        private void Start()
        {
            _material = GetComponent<MeshRenderer>().materials[0];
        }

        protected override void SetView()
        {
            Color color = new Color(_rgbPower.x * _audioBandValue, _rgbPower.y * _audioBandValue, _rgbPower.z * _audioBandValue);
            _material.SetColor("_EmissionColor", color);
        }
    }
}
