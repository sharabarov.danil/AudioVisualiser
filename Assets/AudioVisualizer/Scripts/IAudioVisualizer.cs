﻿
namespace AudioVisualization
{
    /// <summary>
    /// interface for AudioVisualizer
    /// </summary>
    public interface IAudioVisualizer
    {
        /// <summary>
        /// Set AudioVisualizer
        /// </summary>
        /// <param name="audioVisualizer"></param>
        void SetAudioVisualizer(AudioVisualizer audioVisualizer);
    }
}
