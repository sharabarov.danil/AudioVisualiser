﻿namespace AudioVisualization
{
    /// <summary>
    /// Amplitude
    /// </summary>
    public class Amplitude : IAmplitude
    {
        private float _amplitudeHighest;

        public float GetAmplitude(float[] audioBands)
        {
            float currentAmplitude = 0;

            for (int i = 0; i < audioBands.Length; i++)
            {
                currentAmplitude += audioBands[i];
            }

            if (currentAmplitude > _amplitudeHighest)
            {
                _amplitudeHighest = currentAmplitude;
            }

            return currentAmplitude / _amplitudeHighest;
        }
    }
}
