﻿
namespace AudioVisualization
{
    /// <summary>
    /// Audio Band
    /// </summary>
    public class AudioBand : IAudioBand
    {
        private float[] _freqBandHighest = new float[8];

        public float[] GetAudioBand(float[] frequencyBand)
        {
            float[] audioBands = new float[frequencyBand.Length];

            for (int i = 0; i < frequencyBand.Length; i++)
            {
                if (frequencyBand[i] > _freqBandHighest[i])
                {
                    _freqBandHighest[i] = frequencyBand[i];
                }
                audioBands[i] = frequencyBand[i] / _freqBandHighest[i];
            }
            return audioBands;
        }
    }
}
