﻿using UnityEngine;
namespace AudioVisualization
{

    /// <summary>
    /// Amplitude Rotation
    /// </summary>
    public class AmplitudeRotationVisualizer : AmplitudeVisualizerBase, IAudioVisualizer
    {
        [SerializeField]
        private float _multiplier = 1;
        [SerializeField]
        private bool _xRotation;
        [SerializeField]
        private bool _yRotation;
        [SerializeField]
        private bool _zRotation;

        protected override void SetView()
        {
            transform.Rotate(Time.deltaTime * (_xRotation ? (_audioAmplitudeValue * _multiplier) : 0), Time.deltaTime * (_yRotation ? (_audioAmplitudeValue * _multiplier) : 0), Time.deltaTime * (_zRotation ? (_audioAmplitudeValue * _multiplier) : 0));
        }
    }
}
