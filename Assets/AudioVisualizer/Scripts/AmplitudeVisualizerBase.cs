﻿using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Amplitude Visualizer
    /// </summary>
    public abstract class AmplitudeVisualizerBase : MonoBehaviour, IAudioVisualizer
    {
        [SerializeField]
        protected bool _useBuffer;

        protected AudioVisualizer _audioVisualizer;

        protected float _audioAmplitudeValue => _useBuffer ? _audioVisualizer.AmplitudeBuffer : _audioVisualizer.Amplitude;

        public void SetAudioVisualizer(AudioVisualizer audioVisualizer)
        {
            _audioVisualizer = audioVisualizer;
        }

        protected abstract void SetView();

        protected void Update()
        {
            if (_audioAmplitudeValue > 0)
            {
                SetView();
            }
        }
    }
}
