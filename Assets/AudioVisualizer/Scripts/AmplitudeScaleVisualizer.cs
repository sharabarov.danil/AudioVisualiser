﻿using UnityEngine;
namespace AudioVisualization
{

    /// <summary>
    /// Amplitude Scale
    /// </summary>
    public class AmplitudeScaleVisualizer : AmplitudeVisualizerBase
    {
        [SerializeField]
        private Vector3 _startScale;
        [SerializeField]
        private float _maxScale;
        [SerializeField]
        private bool _xScale;
        [SerializeField]
        private bool _yScale;
        [SerializeField]
        private bool _zScale;

        protected override void SetView()
        {
            transform.localScale = new Vector3(_xScale ? (_audioAmplitudeValue * _maxScale) + _startScale.x : transform.localScale.x, _yScale ? (_audioAmplitudeValue * _maxScale) + _startScale.y : transform.localScale.y, _zScale ? (_audioAmplitudeValue * _maxScale) + _startScale.z : transform.localScale.z);
        }
    }
}
