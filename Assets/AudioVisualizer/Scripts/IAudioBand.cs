﻿namespace AudioVisualization
{
    /// <summary>
    /// Interface for AudioBand
    /// </summary>
    public interface IAudioBand
    {
        /// <summary>
        /// Get Audio Bands
        /// </summary>
        /// <param name="frequencyBand"></param>
        /// <returns></returns>
        float[] GetAudioBand(float[] frequencyBand);
    }
}
