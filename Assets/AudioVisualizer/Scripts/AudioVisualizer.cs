﻿using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// AudioVisualizer
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioVisualizer : MonoBehaviour
    {
        [SerializeField]
        private int _channel = 0;
        [SerializeField]
        private FFTWindow _fftWindow = FFTWindow.Blackman;

        public float[] Samples => _samples;
        public float[] AudioBand => _audioBand;
        public float[] AudioBandBuffer => _audioBandBuffer;
        public float Amplitude => _amplitude;
        public float AmplitudeBuffer => _amplitudeBuffer;

        private float[] _samples = new float[512];
        private float[] _audioBand = new float[8];
        private float[] _audioBandBuffer = new float[8];
        private float _amplitude;
        private float _amplitudeBuffer;

        private float[] _bands = new float[8];
        private float[] _bandsBuffer = new float[8];

        private AudioSource _audioSource;
        private IAudioSamples _audioSamplesData;
        private IFrequencyBand _bandData;
        private IBandBuffer _bandBufferData;
        private IAudioBand _audioBandData;
        private IAudioBand _audioBandBufferData;
        private IAmplitude _amplitudeData;
        private IAmplitude _amplitudeBufferData;

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSamplesData = new AudioSourceSamples(_audioSource, _channel, _fftWindow);
            _bandData = new FrequencyBand();
            _bandBufferData = new BandBuffer();
            _audioBandData = new AudioBand();
            _audioBandBufferData = new AudioBand();
            _amplitudeData = new Amplitude();
            _amplitudeBufferData = new Amplitude();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!_audioSource.isPlaying)
                {
                    _audioSource.Play();
                }
                else
                {
                    _audioSource.Stop();
                }
            }

            if (_audioSource.isPlaying)
            {
                GetSamples();
                GetBands();
                GetBandsBuffer();
                GetAudioBands();
                GetAmplitude();
            }
        }

        private void GetAmplitude()
        {
            _amplitude = _amplitudeData.GetAmplitude(_audioBand);
            _amplitudeBuffer = _amplitudeBufferData.GetAmplitude(_audioBandBuffer);
        }

        private void GetSamples()
        {
            _samples = _audioSamplesData.GetSamples();
        }

        private void GetAudioBands()
        {
            _audioBand = _audioBandData.GetAudioBand(_bands);
            _audioBandBuffer = _audioBandBufferData.GetAudioBand(_bandsBuffer);
        }

        private void GetBandsBuffer()
        {
            _bandsBuffer = _bandBufferData.GetBandBuffer(_bands);
        }

        private void GetBands()
        {
            _bands = _bandData.GetFrequencyBand(_samples);
        }
    }
}
