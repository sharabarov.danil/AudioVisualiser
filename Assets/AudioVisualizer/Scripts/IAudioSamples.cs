﻿namespace AudioVisualization
{
    /// <summary>
    /// interface for getting samples
    /// </summary>
    public interface IAudioSamples
    {
        /// <summary>
        /// Get Samples
        /// </summary>
        /// <returns></returns>
        float[] GetSamples();
    }
}
