﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioVisualization
{
    /// <summary>
    /// Audio Visualizer Controller
    /// </summary>
    public class AudioVisualizerController : MonoBehaviour
    {
        [SerializeField]
        private AudioVisualizer _audioVisualizer;
        private void Start()
        {
            var audioVisualizers = GetComponentsInChildren<IAudioVisualizer>();

            foreach (var item in audioVisualizers)
            {
                item.SetAudioVisualizer(_audioVisualizer);
            }

        }
    }
}
