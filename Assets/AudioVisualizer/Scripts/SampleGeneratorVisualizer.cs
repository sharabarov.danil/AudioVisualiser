﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Example with Cube Generation
    /// </summary>
    public class SampleGeneratorVisualizer : MonoBehaviour, IAudioVisualizer
    {
        [SerializeField]
        private GameObject _sampleCubePrefab;
        [SerializeField]
        private float _radius = 10;

        private AudioVisualizer _audioVisualizer;
        private GameObject[] _sampleCube = new GameObject[512];

        public void SetAudioVisualizer(AudioVisualizer audioVisualizer)
        {
            _audioVisualizer = audioVisualizer;
        }

        private void Start()
        {
            for (int i = 0; i < _sampleCube.Length; i++)
            {
                GameObject instanceSampleCube = Instantiate(_sampleCubePrefab);
                instanceSampleCube.transform.position = this.transform.position;
                instanceSampleCube.transform.parent = this.transform;
                instanceSampleCube.name = "SampleCube" + i;
                this.transform.eulerAngles = new Vector3(0, (-360f * i) / (float)_sampleCube.Length, 0);
                instanceSampleCube.transform.localPosition = transform.forward * _radius;
                _sampleCube[i] = instanceSampleCube;
                instanceSampleCube.GetComponent<SampleVisualizerBase>().Sample = i;
                instanceSampleCube.GetComponent<SampleVisualizerBase>().SetAudioVisualizer(_audioVisualizer);
            }
        }
    }
}
