﻿using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Sample Visualizer
    /// </summary>
    public abstract class SampleVisualizerBase : MonoBehaviour, IAudioVisualizer
    {

        public int Sample = 0;

        protected AudioVisualizer _audioVisualizer;

        protected float _audioSamplesValue => _audioVisualizer.Samples[Sample];

        public void SetAudioVisualizer(AudioVisualizer audioVisualizer)
        {
            _audioVisualizer = audioVisualizer;
        }

        protected abstract void SetView();

        protected void Update()
        {
            if (_audioSamplesValue > 0)
            {
                SetView();
            }
        }
    }
}
