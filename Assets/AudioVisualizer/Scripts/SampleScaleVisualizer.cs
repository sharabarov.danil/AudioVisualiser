﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AudioVisualization
{
    public class SampleScaleVisualizer : SampleVisualizerBase
    {
        [SerializeField]
        private Vector3 _startScale;
        [SerializeField]
        private float _maxScale;
        [SerializeField]
        private bool _xScale;
        [SerializeField]
        private bool _yScale;
        [SerializeField]
        private bool _zScale;
        protected override void SetView()
        {
            transform.localScale = new Vector3(_xScale ? (_audioSamplesValue * _maxScale) + _startScale.x : transform.localScale.x, _yScale ? (_audioSamplesValue * _maxScale) + _startScale.y : transform.localScale.y, _zScale ? (_audioSamplesValue * _maxScale) + _startScale.z : transform.localScale.z);
        }
    }
}
