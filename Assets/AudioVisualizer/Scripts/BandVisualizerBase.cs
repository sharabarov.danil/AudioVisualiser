﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AudioVisualization
{
    /// <summary>
    /// Band Visualizer
    /// </summary>
    public abstract class BandVisualizerBase : MonoBehaviour, IAudioVisualizer
    {
        public int Band;

        [SerializeField]
        protected bool _useBuffer;

        protected AudioVisualizer _audioVisualizer;

        protected float _audioBandValue => _useBuffer ? _audioVisualizer.AudioBandBuffer[Band] : _audioVisualizer.AudioBand[Band];

        public void SetAudioVisualizer(AudioVisualizer audioVisualizer)
        {
            _audioVisualizer = audioVisualizer;
        }

        protected abstract void SetView();

        protected void Update()
        {
            if (_audioBandValue > 0)
            {
                SetView();
            }
        }
    }
}
