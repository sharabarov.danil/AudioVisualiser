﻿namespace AudioVisualization
{
    /// <summary>
    /// Interface for frequency Band
    /// </summary>
    public interface IFrequencyBand
    {
        /// <summary>
        /// Get Frequency band
        /// </summary>
        /// <param name="samples"></param>
        /// <returns></returns>
        float[] GetFrequencyBand(float[] samples);
    }
}
